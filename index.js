﻿
window.datas = {};
window.datas.inputs = {};

for (let input of document.querySelectorAll("[data-cipher]")) {
	let cipher = input.getAttribute("data-cipher");
	window.datas.inputs[cipher] = input;
	input.addEventListener("input", () => {
		transcode(cipher);
	});
}

window.datas.utf16ToArrayBuffer = {
	"utf8": (inUtf16String) => {
		return new TextEncoder("UTF-16").encode(inUtf16String);
	},
	"binary": (inUtf16String) => {
		let sanitized = [...inUtf16String.matchAll("[01]")].join("");
		let out = new Uint8Array(Math.ceil(sanitized.length / 8));
		for (let i = 0; i < sanitized.length; i++) {
			out[Math.floor((sanitized.length - 1 - i) / 8)] += sanitized[sanitized.length - 1 - i] << (i % 8);
		}
		return out;
	},
	"octal": (inUtf16String) => {
		let oct = [...inUtf16String.matchAll("\\d\\d?\\d?")];
		return new Uint8Array(oct.map(value => parseInt(value, 8)));
	},
	"decimal": (inUtf16String) => {
		let dec = [...inUtf16String.matchAll("\\d\\d?\\d?")];
		return new Uint8Array(dec.map(value => parseInt(value)));
	},
	"hexadecimal": (inUtf16String) => {
		let hec = [...inUtf16String.toLowerCase().matchAll("[a-f0-9][a-f0-9]?")];
		return new Uint8Array(hec.map(value => parseInt(value, 16)));
	},
	"base32": (inUtf16String) => {
		let sanitized = [...inUtf16String.matchAll("[a-zA-Z2-7=]")].join("");
		if (base32) {
			return Uint8Array.from(base32.decodeAsBytes(sanitized));
		}
		else
			return new Uint8Array();
	},
	"base64": (inUtf16String) => {
		let sanitized = [...inUtf16String.matchAll("[a-zA-Z0-9+/=]")].join("");
		try {
			return Uint8Array.from(atob(sanitized), c => c.charCodeAt(0));
		} catch (e) {
			return new Uint8Array();
		}
	},
	"ascii85": (inUtf16String) => {
		let sanitized = [...inUtf16String.matchAll("[\\!-u]")].join("");
		if (ascii85) {
			return Uint8Array.from(ascii85.decode(sanitized));
		}
		else
			return new Uint8Array();
	},
}

window.datas.ArrayBufferToUtf16 = {
	"utf8": (buffer) => {
		return new TextDecoder("UTF-8").decode(buffer);
	},
	"binary": (buffer) => {
		return [...buffer].map((byte) => { return byte.toString(2).padStart(8, 0); }).join(" ");
	},
	"octal": (buffer) => {
		return [...buffer].map((byte) => byte.toString(8)).join(" ");
	},
	"decimal": (buffer) => {
		return buffer.join(" ");
	},
	"hexadecimal": (buffer) => {
		return [...buffer].map((byte) => byte.toString(16).padStart(2, 0)).join(" ");
	},
	"base32": (buffer) => {
		if (base32) {
			return base32.encodeBytes(buffer);
		}
	},
	"base64": (buffer) => {
		return btoa(String.fromCharCode(...buffer));
	},
	"ascii85": (buffer) => {
		if (ascii85) {
			return ascii85.encode([...buffer]);
		}
	},
	"infos": async (buffer) => {
		if (CryptoJS) {
			let asUtf16 = new TextDecoder("UTF-8").decode(buffer);
			let sha1 = [...new Uint8Array(await crypto.subtle.digest("SHA-1", buffer))].map((byte) => byte.toString(16).padStart(2, 0)).join("").toUpperCase();
			let sha256 = [...new Uint8Array(await crypto.subtle.digest("SHA-256", buffer))].map((byte) => byte.toString(16).padStart(2, 0)).join("").toUpperCase();
			let sha384 = [...new Uint8Array(await crypto.subtle.digest("SHA-384", buffer))].map((byte) => byte.toString(16).padStart(2, 0)).join("").toUpperCase();
			let sha512 = [...new Uint8Array(await crypto.subtle.digest("SHA-512", buffer))].map((byte) => byte.toString(16).padStart(2, 0)).join("").toUpperCase();

			return "" +
				`Bytes: ${buffer.length}
Characters: ${[...asUtf16].length}
Words: ${[...asUtf16.matchAll("\\S+")].length}

MD5 : ${md5(buffer)}

SHA-1: ${sha1}

SHA-256: ${sha256}

SHA-384: ${sha384}

SHA-512: ${sha512}
`;
		}
	}
}

async function transcode(fromCipher) {
	let buffer = window.datas.utf16ToArrayBuffer[fromCipher](window.datas.inputs[fromCipher].value);

	for (let toCipher in window.datas.ArrayBufferToUtf16) {
		if (toCipher != fromCipher)
			window.datas.inputs[toCipher].value = await window.datas.ArrayBufferToUtf16[toCipher](buffer);
	}
}
